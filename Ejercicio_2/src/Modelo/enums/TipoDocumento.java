/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Enum.java to edit this template
 */
package Modelo.enums;

/**
 *
 * @author Usuario
 */
public enum TipoDocumento {
    TICKET("ticket"), CUENTAS("CUENTAS"),TARJETAS("tarjetas"),PRESTAMOS("prestamos");
    private String tipo;

    private TipoDocumento(String tipo) {
        this.tipo = tipo;
    }
    public String getTipo() {
        return this.tipo;
    }
}
