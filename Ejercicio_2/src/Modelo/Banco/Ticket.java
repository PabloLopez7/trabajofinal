/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo.Banco;

/**
 *
 * @author Usuario
 */
public class Ticket {
    private String Atencion_Oficinas;
    private String Cuentas;
    private String Tarjeta_Debito;
    private String Trajeta_Credito;
    private String Prestamos;

    public String getAtencion_Oficinas() {
        return Atencion_Oficinas;
    }

    public void setAtencion_Oficinas(String Atencion_Oficinas) {
        this.Atencion_Oficinas = Atencion_Oficinas;
    }

    public String getCuentas() {
        return Cuentas;
    }

    public void setCuentas(String Cuentas) {
        this.Cuentas = Cuentas;
    }

    public String getTarjeta_Debito() {
        return Tarjeta_Debito;
    }

    public void setTarjeta_Debito(String Tarjeta_Debito) {
        this.Tarjeta_Debito = Tarjeta_Debito;
    }

    public String getTrajeta_Credito() {
        return Trajeta_Credito;
    }

    public void setTrajeta_Credito(String Trajeta_Credito) {
        this.Trajeta_Credito = Trajeta_Credito;
    }

    public String getPrestamos() {
        return Prestamos;
    }

    public void setPrestamos(String Prestamos) {
        this.Prestamos = Prestamos;
    }
    
    
}
