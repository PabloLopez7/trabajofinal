/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Banco;

import Controlador.ListaEnlazadaService;
import Modelo.Banco.Ticket;

/**
 *
 * @author Usuario
 */
public class BancoController {
    private Ticket ticket;
    private ListaEnlazadaService<Ticket> listaTicket;

    public ListaEnlazadaService<Ticket> getListaTicket() {
        if(this.listaTicket==null)
            this.listaTicket = new ListaEnlazadaService<>();
        return listaTicket;
    }

    public void setListaTicket(ListaEnlazadaService<Ticket> listaTicket) {
        this.listaTicket = listaTicket;
    }
    
    public Ticket getTicket() {
        if(this.ticket==null)
            this.ticket=new Ticket();
        return ticket;
    }

    public void setTicket(Ticket ticket) {
        this.ticket = ticket;
    }

    
    
    
}
