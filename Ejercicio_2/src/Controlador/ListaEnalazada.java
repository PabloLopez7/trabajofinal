/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Controlador.Exception.PosicionException;

/**
 *
 * @author Usuario
 */
public class ListaEnalazada<E> {

    private NodoLista<E> cabecera;
    private Integer size;

    public ListaEnalazada() {

        cabecera = null;
        size = 0;
    }

    public Boolean estaVacia() {
        return cabecera == null;
    }

    public void Insertar(E dato) {
        NodoLista<E> nuevo = new NodoLista<>(dato, null);
        if (estaVacia()) {
            cabecera = nuevo;
        } else {
            NodoLista<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }

    public void insertarCabecera(E dato) {
        if (estaVacia()) {
            Insertar(dato);
        } else {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);
            System.out.println("pos==0");
            nuevo.setSiguiente(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void Insertar(E dato, Integer pos) throws PosicionException {

        if (estaVacia()) {
            Insertar(dato);
        } else if (pos >= 0 && pos <= size) {
            NodoLista<E> nuevo = new NodoLista<>(dato, null);
            if (pos == (size - 1)) {
                Insertar(dato);
                System.out.println("pos==size -1");
            } else {
                NodoLista<E> aux = cabecera;
                for (int i = 0; i < pos; i++) {
                    aux = aux.getSiguiente();
                }
                NodoLista<E> siguiente = aux.getSiguiente();
                aux.setSiguiente(nuevo);
                nuevo.setSiguiente(siguiente);
                size++;
            }
        } else {
            throw new PosicionException("No existe la posicion exacta");
        }
    }

    public void imprimit() {
        System.out.println("*********************************");
        NodoLista<E> aux = cabecera;
        for (int i = 0; i < getSize(); i++) {
            System.out.println(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
        }
        System.out.println("\n" + "**********************************");
    }

    public Integer getSize() {
        return size;
    }

    public E obtenerDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos <= size) {
                E dato = null;
                if (pos == 0) {
                    dato = cabecera.getDato();
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    dato = aux.getDato();
                }

                return dato;
            } else {
                throw new PosicionException("No existe la posicion exacta");
            }
        } else {
            throw new PosicionException("La lista esta vacia por ender no hay datos");
        }

    }

    public void EliminarDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos <= size) {
                if (pos == 0) {
                    cabecera = cabecera.getSiguiente();
                    size--;
                } else {
                    NodoLista<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    NodoLista<E> proximo = aux.getSiguiente();
                    aux.setSiguiente(proximo.getSiguiente());
                    size--;
                }
            } else {
                throw new PosicionException("Error de eliminar dato");
            }
        } else {
            throw new PosicionException("Eliminar daos: La lista esta vacia por ender no hay datos");
        }
    }

    public void vaciar() {
        cabecera = null;
        size = 0;
    }
}