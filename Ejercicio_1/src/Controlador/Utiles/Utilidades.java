/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Utiles;

import Modelo.Comandos.Comandos;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 *
 * @author Usuario
 */
public class Utilidades {

    public static Field getField(String name, Class clazz) {
        Field[] fields = Comandos.class.getFields();
        Field aux = null;
        for (int i = 0; i < fields.length; i++) {
            
            if (name.toString().equalsIgnoreCase(fields[i].getName())) {
                aux = fields[i];
                break;
            }
        }
        if(aux != null) return aux;
            
        fields = clazz.getSuperclass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
           
            if (name.toString().equalsIgnoreCase(fields[i].getName())) {
                aux = fields[i];
                break;
            }
        }
        return aux;
    }

    public static Method getMethod(String name, Class clazz) {

        Method[] methods = clazz.getDeclaredMethods();
        Method aux = null;

        for (int i = 0; i < methods.length; i++) {
            System.out.println(methods[i].getName());
            if (name.toString().equalsIgnoreCase(methods[i].getName())) {
                aux = methods[i];
                break;
            }
        }
        if(aux != null) return aux;
        methods = clazz.getSuperclass().getDeclaredMethods();
        for (int i = 0; i < methods.length; i++) {
            System.out.println(methods[i].getName());
            if (name.toString().equalsIgnoreCase(methods[i].getName())) {
                aux = methods[i];
                break;
            }
        }
        return aux;

    }

    public static Object cambiarDatos(Object dato, String field, Comandos a) throws Exception {

        Field fieldA = getField(field, a.getClass());
        char[] arr = field.toCharArray();
        arr[0] = Character.toUpperCase(arr[0]);
        Method method = getMethod("set" + new String(arr), a.getClass());
        try {
            if(fieldA.getType().getSuperclass().getSimpleName().equalsIgnoreCase("Number")){
                method.invoke(a, transformarDatoNumber(fieldA.getType(), dato.toString()));
            } else {
                method.invoke(a, dato);
            }
            
        } catch (Exception e) {
            System.out.println(e + "  " + field);
        }
        return a;
    }
    
    public static Number transformarDatoNumber(Class type, String dato){
        Number number=null;
        if(type.getSimpleName().equalsIgnoreCase(Integer.class.getSimpleName())){
            number= Integer.parseInt(dato);
        }
        if(type.getSimpleName().equalsIgnoreCase(Double.class.getSimpleName())){
            number= Double.parseDouble(dato);
        }
        if(type.getSimpleName().equalsIgnoreCase(Float.class.getSimpleName())){
            number= Float.parseFloat(dato);
        }
        if(type.getSimpleName().equalsIgnoreCase(Short.class.getSimpleName())){
            number= Short.parseShort(dato);
        }
        return number;
    }
}
