/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Controlador.Exception.PosicionException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
@XmlRootElement

public class ListaEnlazada<E> {

    private Nodo<E> cabecera;
    private Integer size;
    @XmlElement
    public Nodo<E> getCabecera() {
        return cabecera;
    }

    public void setCabecera(Nodo<E> cabecera) {
        this.cabecera = cabecera;
    }
    
    public ListaEnlazada() {

        cabecera = null;
        size = 0;
    }

    public Boolean estaVacia() {
        return cabecera == null;
    }

    public void Insertar(E dato) {
        Nodo<E> nuevo = new Nodo<>(dato, null);
        if (estaVacia()) {
            cabecera = nuevo;
        } else {
            Nodo<E> aux = cabecera;
            while (aux.getSiguiente() != null) {
                aux = aux.getSiguiente();
            }
            aux.setSiguiente(nuevo);
        }
        size++;
    }

    public void insertarCabecera(E dato) {
        if (estaVacia()) {
            Insertar(dato);
        } else {
            Nodo<E> nuevo = new Nodo<>(dato, null);
            nuevo.setSiguiente(cabecera);
            cabecera = nuevo;
            size++;
        }
    }

    public void Insertar(E dato, Integer pos) throws PosicionException {

        if (estaVacia()) {
            Insertar(dato);
        } else if (pos >= 0 && pos <= size) {
            Nodo<E> nuevo = new Nodo<>(dato, null);
            if (pos == (size - 1)) {
                Insertar(dato);
                System.out.println("pos==size -1");
            } else {
                Nodo<E> aux = cabecera;
                for (int i = 0; i < pos; i++) {
                    aux = aux.getSiguiente();
                }
                Nodo<E> siguiente = aux.getSiguiente();
                aux.setSiguiente(nuevo);
                nuevo.setSiguiente(siguiente);
                size++;
            }
        } else {
            throw new PosicionException("No existe la posicion exacta");
        }
    }

    public void imprimit() {
        System.out.println("*********************************");
        Nodo<E> aux = cabecera;
        for (int i = 0; i < getSize(); i++) {
            System.out.println(aux.getDato().toString() + "\t");
            aux = aux.getSiguiente();
        }
        System.out.println("\n" + "**********************************");
    }

    public Integer getSize() {
        return size;
    }

    public E obtenerDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos <= size) {
                E dato = null;
                if (pos == 0) {
                    dato = cabecera.getDato();
                } else {
                    Nodo<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    dato = aux.getDato();
                }

                return dato;
            } else {
                throw new PosicionException("No existe la posicion exacta");
            }
        } else {
            throw new PosicionException("La lista esta vacia por ender no hay datos");
        }

    }

    public void EliminarDato(Integer pos) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos <= size) {
                if (pos == 0) {
                    cabecera = cabecera.getSiguiente();
                    size--;
                } else {
                    Nodo<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    Nodo<E> proximo = aux.getSiguiente();
                    aux.setSiguiente(proximo.getSiguiente());
                    size--;
                }
            } else {
                throw new PosicionException("Error de eliminar dato");
            }
        } else {
            throw new PosicionException("Eliminar daos: La lista esta vacia por ender no hay datos");
        }
    }

    public void vaciar() {
        cabecera = null;
        size = 0;
    }
    
    public void ModificarDato(Integer pos, E datoM) throws PosicionException {
        if (!estaVacia()) {
            if (pos >= 0 && pos <= size) {
                //E dato = null;
                if (pos == 0) {
                    cabecera.setDato(datoM);
                } else {
                    Nodo<E> aux = cabecera;
                    for (int i = 0; i < pos; i++) {
                        aux = aux.getSiguiente();
                    }
                    aux.setDato(datoM);
                }
            } else {
                throw new PosicionException("No existe la posicion exacta");
            }
        } else {
            throw new PosicionException("La lista esta vacia por ender no hay datos");
        }

    }

}
