/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Comandos.Servicios;

import Controlador.Comandos.ComandoController;
import Controlador.ListaEnlazadaServices;
import Modelo.Comandos.Comandos;

/**
 *
 * @author Usuario
 */
public class ComandoServicios {
    private ComandoController ac = new ComandoController();
    public Comandos getComando(){
        return ac.getComandos();
    }
    
    public ListaEnlazadaServices<Comandos> getLista() {
        return ac.getListaComandos();
    }
    
    public ListaEnlazadaServices<Comandos> getListaArchivo() {
        return ac.listado();
    }
    
    public Boolean guardar(){
        return ac.guardar();
    }
    
    public Boolean modificar(Integer pos){
        return ac.modificar(pos);
    }
      
    public void setComandos(Comandos comando){
        ac.setComandos(comando);
    }

}
