/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Comandos;

import Controlador.DAO.AdaptadorDao;
import Controlador.Exception.PosicionException;
import Controlador.ListaEnlazada;
import Controlador.ListaEnlazadaServices;
import Controlador.Utiles.Utilidades;
import Modelo.Comandos.Comandos;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Usuario
 */
public class ComandoController extends AdaptadorDao<Comandos> {

    private Comandos comando;
    private ListaEnlazadaServices<Comandos> listaComandos;

    public ComandoController() {
        super(Comandos.class);
        listado();
    }

    public ListaEnlazadaServices<Comandos> getListaComandos() {
        if(this.listaComandos==null){
            listado();
        }
        return listaComandos;
    }

    public void setListaComandos(ListaEnlazadaServices<Comandos> listaComandos) {
        this.listaComandos = listaComandos;
    }

    public Comandos getComandos() {
        if (this.comando == null) {
            this.comando = new Comandos();
        }
        return comando;
    }

    public void setComandos(Comandos comandos) {
        this.comando = comandos;
    }

    public Boolean guardar() {
        try {
            getComandos().setId(listaComandos.getSize() + 1);
            guardar(getComandos());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar" + e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {
            
            modificar(getComandos(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar" + e);
        }
        return false;
    }

    public ListaEnlazadaServices<Comandos> listado() {
        setListaComandos(listar());
        return listaComandos;
    }
}
