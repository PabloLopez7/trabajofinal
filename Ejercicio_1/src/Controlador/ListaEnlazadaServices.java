/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Controlador.Exception.PosicionException;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Usuario
 */
//@XmlAccessorType(XmlAccessType.FIELD)
public class ListaEnlazadaServices <E>{
   private ListaEnlazada<E> lista;

    public ListaEnlazada<E> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazada<E> lista) {
        this.lista = lista;
    }
    
    public ListaEnlazadaServices(){
        this.lista=new ListaEnlazada<>();
    }
   
    public Boolean insertarAlInciio(E dato){
            lista.insertarCabecera(dato);
            return true;
    }
    
    public Boolean insertarAlFinal(E dato){
        try{
            lista.Insertar(dato, lista.getSize()-1);
            return true;
        }catch(PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public Boolean insetar(E dato,Integer pos){
        try{
            lista.Insertar(dato, pos);
            return true;
        }catch(PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public Integer getSize(){
        return lista.getSize();
    }
    
    public E obtenerDato(Integer pos){
        try{
         return lista.obtenerDato(pos);   
        }catch (Exception e){
            System.out.println(e);
            return null;
        }
    }
    
    public Boolean eliminarCabecera(){
        try{
            lista.EliminarDato(0);
            return true;
        }catch (PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public Boolean eliminarUltimo(){
        try{
            lista.EliminarDato(getSize()-1);
            return true;
        }catch (PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public Boolean eliminarPosicion(Integer pos){
        try{
            lista.EliminarDato(pos);
            return true;
        }catch (PosicionException e){
            System.out.println(e);
        }
        return false;
    }
    
    public Boolean modificarDatoPosicion(Integer pos,E dato){
        try{
            lista.ModificarDato(pos, dato);
            return true;
        }catch (PosicionException e){
            System.out.println(e);
        }
        return false;
    } 
    
    public void limpiarLista(){
        lista.vaciar();
    }
}
