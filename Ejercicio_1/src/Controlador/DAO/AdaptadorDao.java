/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.DAO;

import Controlador.ListaEnlazada;
import Controlador.ListaEnlazadaServices;
import Controlador.Utiles.Utilidades;
import Modelo.Comandos.Comandos;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Usuario
 */
public class AdaptadorDao<T> implements InterfazDAO<T> {

    private Class<T> clazz;
    private String URL = "dato" + File.separatorChar;

    public AdaptadorDao(Class<T> clazz) {
        this.clazz = clazz;
        URL += this.clazz.getSimpleName() + ".xml";
    }

    @Override
    public void guardar(T dato) throws Exception {
        ListaEnlazada<T> lista = listar().getLista();
        if(lista.getSize()!=0){
          lista.Insertar(dato, lista.getSize() - 1);  
        }else{
            lista.Insertar(dato,0);
        }
        FileOutputStream file = new FileOutputStream(URL);
        JAXBContext jAXBContext = JAXBContext.newInstance(new Class[]{ListaEnlazada.class, this.clazz});
        Marshaller marshaller = jAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(lista, file);
    }

    @Override
    public void modificar(T dato, Integer pos) throws Exception {
        ListaEnlazada<T> lista = listar().getLista();
        lista.ModificarDato(pos, dato);
        FileOutputStream file = new FileOutputStream(URL);
        JAXBContext jAXBContext = JAXBContext.newInstance(new Class[]{ListaEnlazada.class, this.clazz});
        Marshaller marshaller = jAXBContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(lista, file);
    }

    @Override
    public ListaEnlazadaServices<T> listar() {
        ListaEnlazada<T> lista = new ListaEnlazada<>();
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newDefaultInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            FileInputStream xmlFile = new FileInputStream(URL);
            ByteArrayInputStream bis = new ByteArrayInputStream(xmlFile.readAllBytes());
            Document doc = db.parse(bis);
            NodeList datos = doc.getElementsByTagName(this.clazz.getSimpleName().toLowerCase());
            for (int i = 0; i < datos.getLength(); i++) {
                Node n1 = datos.item(i);

                NodeList aux = n1.getChildNodes();
                T objeto = this.clazz.newInstance();
                for (int j = 0; j < aux.getLength(); j++) {
                    Node dato = aux.item(j);

                    if (dato.getNodeName() != null && !dato.getNodeName().equals("")
                            && dato.getTextContent() != null && dato.getTextContent().equals("") && !dato.getNodeName().equals("#text")) {
                        //construir el objeto
                        objeto = (T) Utilidades.cambiarDatos(dato.getTextContent(), dato.getNodeName(), (Comandos) objeto);
                        
                    }
                }
                lista.Insertar(objeto, lista.getSize() - 1);
            }
            lista.imprimit();
        } catch (Exception e) {
            System.out.println("NO SE PUEDE CARGAR "+e);
        }
        ListaEnlazadaServices<T> listado = new ListaEnlazadaServices<>();
        listado.setLista(lista);
        System.out.println("Hola listado");
        return listado;
    }

}
