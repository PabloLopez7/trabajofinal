/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.DAO;

import Controlador.ListaEnlazada;
import Controlador.ListaEnlazadaServices;

/**
 *
 * @author Usuario
 */
public interface InterfazDAO <T> {
    public void guardar(T dato) throws Exception;
    public void modificar(T dato, Integer pos) throws Exception;
    public ListaEnlazadaServices<T> listar();
}
