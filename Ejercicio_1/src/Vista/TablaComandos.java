/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista;

import Controlador.ListaEnlazadaServices;
import Modelo.Comandos.Comandos;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaComandos extends AbstractTableModel{
    private ListaEnlazadaServices<Comandos> lista;

    public ListaEnlazadaServices<Comandos> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Comandos> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getColumnCount(){
        return 3;
    }
    
    @Override
    public int getRowCount(){
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            case 0: return "Nro";
            case 1: return "Comando";
            case 2: return "Fecha";
            default: return null; 
        }
    }
    
    @Override
    public Object getValueAt(int arg0, int arg1){
        Comandos comando = lista.obtenerDato(arg0);
        switch(arg1){
            case 0: return (arg0+1);
            case 1: return comando.getComandos();
            case 2: return comando.getFecha();
            default: return null; 
        }
    }
}
