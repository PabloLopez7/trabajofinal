/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.DAO;

import Controlador.DAO.InterfazDao;
import Controlador.ListaEnlazada;
import Controlador.ListaEnlazadaServices;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.json.JettisonMappedXmlDriver;
import com.thoughtworks.xstream.security.AnyTypePermission;
import com.thoughtworks.xstream.security.NullPermission;
import com.thoughtworks.xstream.security.PrimitiveTypePermission;
import controlador.tda.cola.Cola;
import controlador.tda.cola.ColaServices;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;

/**
 *
 * @author Usuario
 */
public class AdaptadorDao <T> implements InterfazDao<T> {
    private XStream xstream;
    private Class<T> clazz;
    private  String URL = "datos" + File.separatorChar;

    public AdaptadorDao(Class<T> clazz) {
        this.clazz = clazz;
        URL += this.clazz.getSimpleName() + ".json";
        xstream = new XStream(new JettisonMappedXmlDriver());
        xstream.alias("lista", ListaEnlazada.class);        
        xstream.setMode(XStream.NO_REFERENCES);
        
        xstream.addPermission(AnyTypePermission.ANY);
        
        xstream.addPermission(NullPermission.NULL);   // allow "null"
        xstream.addPermission(PrimitiveTypePermission.PRIMITIVES); // allow primitive types
    }

    @Override
    public void guardar(T dato) throws Exception {
        ColaServices<T> lista = listar();
        lista.queue(dato);
       // xstream.alias(clazz.getSimpleName().toLowerCase(), clazz);
        
        xstream.toXML(lista.getCola(), new FileOutputStream(URL));
    }

    @Override
    public void modificar(T dato, Integer pos) throws Exception {
        ColaServices<T> lista = listar();
        lista.getCola().modificarDato(pos, dato);       
        xstream.toXML(lista.getCola(), new FileOutputStream(URL));
    }

    @Override
    public ColaServices<T> listar() {
        
        ColaServices<T> listaAux = new ColaServices<T>(0);
        try {
            listaAux.setCola((Cola<T>)xstream.fromXML(new FileReader(URL)));
        } catch (Exception e) {
            System.out.println("ERROR "+e);
            //e.printStackTrace();
        }
        return listaAux;
    }
    
    
}
