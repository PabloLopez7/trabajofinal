/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.DAO;

import Controlador.ListaEnlazadaServices;
import controlador.tda.cola.ColaServices;

/**
 *
 * @author Usuario
 */
public interface InterfazDao <T> {
   public void guardar(T dato) throws Exception;
    public void modificar(T dato, Integer pos) throws Exception;
    public ColaServices<T> listar();
} 

