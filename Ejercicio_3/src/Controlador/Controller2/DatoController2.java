/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Controller2;

import Controlador.ListaEnlazadaServices;
import Modelo.Datos;

/**
 *
 * @author Usuario
 */
public class DatoController2 {
    private Datos datos;
    private ListaEnlazadaServices<Datos> listaDatos;

    public ListaEnlazadaServices<Datos> getListaDato() {
        if(this.listaDatos==null)
            this.listaDatos=new ListaEnlazadaServices<>() ;
        return listaDatos;
    }

    public void setListaPelicula(ListaEnlazadaServices<Datos> listaDatos) {
        this.listaDatos = listaDatos;
    }

    public Datos getDatos() {
        if(this.datos==null)
            this.datos = new Datos();
        return datos;
    }

    public void setPelicula(Datos datos) {
        this.datos = datos;
    }
}
