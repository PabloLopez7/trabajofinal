/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Controller2;

import Controlador.ListaEnlazadaServices;
import Modelo.Fuente;


/**
 *
 * @author Usuario
 */
public class FuenteController2 {
    private Fuente fuente;
    private ListaEnlazadaServices<Fuente> listaFuente;

    public ListaEnlazadaServices<Fuente> getListaFuente() {
        if(this.listaFuente==null)
            this.listaFuente=new ListaEnlazadaServices<>() ;
        return listaFuente;
    }

    public void setListaPelicula(ListaEnlazadaServices<Fuente> listaFuente) {
        this.listaFuente = listaFuente;
    }

    public Fuente getFuente() {
        if(this.fuente==null)
            this.fuente=new Fuente();
        return fuente;
    }

    public void setPelicula(Fuente fuente) {
        this.fuente = fuente;
    }
}
