/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Controller2;

import Controlador.ListaEnlazadaServices;
import Modelo.Pelicula;

/**
 *
 * @author Usuario
 */
public class PeliculaController2 {
    private Pelicula pelicula;
    private ListaEnlazadaServices<Pelicula> listaPelicula;

    public ListaEnlazadaServices<Pelicula> getListaPelicula() {
        if(this.listaPelicula==null)
            this.listaPelicula=new ListaEnlazadaServices<>() ;
        return listaPelicula;
    }

    public void setListaPelicula(ListaEnlazadaServices<Pelicula> listaPelicula) {
        this.listaPelicula = listaPelicula;
    }

    public Pelicula getPelicula() {
        if(this.pelicula==null)
            this.pelicula=new Pelicula();
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }
    
    
}
