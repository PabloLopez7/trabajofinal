/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.tda.cola;

import Controlador.TDA.Exception.EstructuraDataVaciaExpetion;
import Controlador.TDA.Exception.PosicionException;
import Controlador.TDA.Exception.TopeException;
/**
 *
 * @author sebastian
 */
public class ColaServices <E> {
    private Cola<E> cola;

    public ColaServices(Integer tope) {
       cola = new Cola<>(tope);
    }
    
    public boolean queue(E dato) {
        try {
            cola.queue(dato);
            return true;
        } catch (TopeException | PosicionException e) {
            System.out.println("Error "+e);
        }
        return false;
    }
    
    public E dequeue(Integer pos) {
        try {
            return cola.dequeue(pos);
            
        } catch (EstructuraDataVaciaExpetion | PosicionException e) {
            System.out.println("Error "+e);
        }
        return null;
    }
    
    public Integer getSize() {
        return cola.getSize();
    }
    
    public Integer getTope() {
        return cola.getTope();
    }

    public Cola<E> getCola() {
        return cola;
    }

    public void setCola(Cola<E> cola) {
        this.cola = cola;
    }
    
}
