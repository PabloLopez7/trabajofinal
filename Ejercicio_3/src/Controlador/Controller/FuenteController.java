/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Controller;

import Controlador.DAO.AdaptadorDao;
import Controlador.ListaEnlazadaServices;
import Modelo.Fuente;
import Modelo.Pelicula;
import controlador.tda.cola.ColaServices;

/**
 *
 * @author Usuario
 */
public class FuenteController extends AdaptadorDao<Fuente> {
    
    private Fuente fuente;
    private ColaServices<Fuente> lista = new ColaServices<>(0);
    
    public FuenteController(){
        super(Fuente.class);
        listado();
    }

    public Fuente getFuente() {
        if(fuente == null)
            fuente = new Fuente();
        return fuente;
    }

    public void setFuente(Fuente fuente) {
        this.fuente = fuente;
    }

    public ColaServices<Fuente> getLista() {
        return lista;
    }

    public void setLista(ColaServices<Fuente> lista) {
        this.lista = lista;
    }
    
    public Boolean guardar() {
        try {            
            getFuente().getSource();
            guardar(getFuente());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar Fuente"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getFuente(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar Fuente"+e);
        }
        return false;
    }
    
    public ColaServices<Fuente> listado() {
        setLista(listar());
        return lista;
    }
    
    public static void main(String[] args){
        FuenteController fc = new FuenteController();
        fc.getFuente().setSource("Internet Movie Database");
        fc.getFuente().setValue("7.6/10");
        
        fc.guardar();
        fc.setFuente(null);
        
        fc.getFuente().setSource("Rotten Tomatoes");
        fc.getFuente().setValue("85%");
        
        fc.guardar();
        fc.setFuente(null);
        
        fc.getFuente().setSource("Metacritic");
        fc.getFuente().setValue("67.0/100");
        
        fc.guardar();
       
        
    }
}