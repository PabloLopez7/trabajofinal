
package Controlador.Controller;

import Controlador.DAO.AdaptadorDao;
import Controlador.ListaEnlazadaServices;
import Modelo.Datos;
import controlador.tda.cola.ColaServices;

public class DatosController extends AdaptadorDao<Datos> {
    
    private Datos datos;
    private ColaServices<Datos> lista = new ColaServices<>(0);
    
    public DatosController(){
        super(Datos.class);
        listado();
    }

    public Datos getDatos() {
        if(datos == null)
            datos = new Datos();
        return datos;
    }

    public void setDatos(Datos datos) {
        this.datos = datos;
    }

    public ColaServices<Datos> getLista() {
        return lista;
    }

    public void setLista(ColaServices<Datos> lista) {
        this.lista = lista;
    }
    
    public Boolean guardar() {
        try {            
            getDatos().setMetascore("67");
            guardar(getDatos());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar Datos"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getDatos(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar Datos"+e);
        }
        return false;
    }
    
    public ColaServices<Datos> listado() {
        setLista(listar());
        return lista;
    }
    
    public static void main(String[] args){
        DatosController dc = new DatosController();
        dc.getDatos().setMetascore("67");
        dc.getDatos().setImdbRating("7.6");
        dc.getDatos().setImdbVotes("650.596");
        dc.getDatos().setImdbID("tt3896198");
        dc.getDatos().setType("movie");
        dc.getDatos().setDVD("22 Aug 2017");
        dc.getDatos().setBoxOffice("$389,813,101");
        dc.getDatos().setProduction("N/A");
        dc.getDatos().setWebsite("N/A");
        dc.getDatos().setResponse("True");
        dc.guardar();
        
       
        
    }
    
}
