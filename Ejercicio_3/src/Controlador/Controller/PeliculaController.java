/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador.Controller;

import Controlador.DAO.AdaptadorDao;
import Controlador.ListaEnlazadaServices;
import Modelo.Pelicula;
import controlador.tda.cola.ColaServices;

/**
 *
 * @author Usuario
 */
public class PeliculaController extends AdaptadorDao<Pelicula> {
    
    private Pelicula pelicula;
    private ColaServices<Pelicula> lista = new ColaServices<>(0);
    
    public PeliculaController(){
        super(Pelicula.class);
        listado();
    }

    public Pelicula getPelicula() {
        if(pelicula == null)
            pelicula = new Pelicula();
        return pelicula;
    }

    public void setPelicula(Pelicula pelicula) {
        this.pelicula = pelicula;
    }

    public ColaServices<Pelicula> getLista() {
        return lista;
    }

    public void setLista(ColaServices<Pelicula> lista) {
        this.lista = lista;
    }
    
    public Boolean guardar() {
        try {            
            getPelicula();
            guardar(getPelicula());
            return true;
        } catch (Exception e) {
            System.out.println("Error en guardar Pelicula"+e);
        }
        return false;
    }
    
    public Boolean modificar(Integer pos) {
        try {            
            
            modificar(getPelicula(), pos);
            return true;
        } catch (Exception e) {
            System.out.println("Error en modificar pelicula"+e);
        }
        return false;
    }
    
    public ColaServices<Pelicula> listado() {
        setLista(listar());
        return lista;
    }
    
    public static void main(String[] args){
        PeliculaController pc = new PeliculaController();
        pc.getPelicula().setTitle("Guardians of the Galaxy Vol. 2");
        pc.getPelicula().setYear("2017");
        pc.getPelicula().setRated("PG-13");
        pc.getPelicula().setReleased("05-may-2017");
        pc.getPelicula().setRuntime("136 min");
        pc.getPelicula().setGenre("Action, Adventure, Comedy");
        pc.getPelicula().setDirector("James Gunn");
        pc.getPelicula().setWrite("James Gunn, Dan Abnett, Andy Lanning");
        pc.getPelicula().setActors("Chris Pratt, Zoe Saldana, Dave Bautista");
        pc.getPelicula().setPlot("The Guardians struggle to keep together as a team while dealing with their personal family issues, notably Star-Lord's encounter with his father the ambitious celestial being Ego");
        pc.getPelicula().setLanguaje("English");
        pc.getPelicula().setCountry("United States");
        pc.getPelicula().setAwards("Nominated for 1 Oscar. 15 wins & 59 nominations total");
        pc.getPelicula().setPoster("https://m.media-amazon.com/images/M/MV5BNjM0NTc0NzItM2FlYS00YzEwLWE0YmUtNTA2ZWIzODc2OTgxXkEyXkFqcGdeQXVyNTgwNzIyNzg@._V1_SX300.jpg");
        pc.guardar();    
    }
}
