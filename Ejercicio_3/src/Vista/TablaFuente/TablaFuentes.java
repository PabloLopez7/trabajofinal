/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista.TablaFuente;

import Controlador.ListaEnlazadaServices;
import Modelo.Fuente;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaFuentes extends AbstractTableModel{
    private ListaEnlazadaServices<Fuente> lista2;

    public ListaEnlazadaServices<Fuente> getLista() {
        return lista2;
    }

    public void setLista(ListaEnlazadaServices<Fuente> lista) {
        this.lista2 = lista;
    }
    
    @Override
    public int getColumnCount(){
        return 3;
    }
    
     @Override
    public int getRowCount(){
        return lista2.getSize();
    }
    @Override
    public String getColumnName(int column){
        switch(column){
            
            case 0: return "Nro";
            case 1: return "Source";
            case 2: return "Value";
            default: return null; 
        }
    }
    
    @Override
    public Object getValueAt(int arg0, int arg1){
        Fuente fuente = lista2.obtenerDato(arg0);
        switch(arg1){   
            case 0: return (arg0+1);
            case 1: return fuente.getSource();
            case 2: return fuente.getValue();
            default: return null; 
        }
    }
}
