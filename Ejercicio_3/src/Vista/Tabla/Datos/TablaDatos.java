/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista.Tabla.Datos;

import Controlador.ListaEnlazadaServices;
import Modelo.Datos;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaDatos extends AbstractTableModel {
    private ListaEnlazadaServices<Datos> lista;

    public ListaEnlazadaServices<Datos> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Datos> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getColumnCount(){
        return 11;
    }
    
     @Override
    public int getRowCount(){
        return lista.getSize();
    }
    @Override
    public String getColumnName(int column){
        switch(column){
            
            case 0: return "Nro";
            case 1: return "Metascore";
            case 2: return "imdbRating";
            case 3: return "imdbVotes";
            case 4: return "imdbID";
            case 5: return "Type";
            case 6: return "DVD";
            case 7: return "BoxOffice";
            case 8: return "Production";
            case 9: return "Website";
            case 10: return "Response";
            
            default: return null; 
        }
    }
    
    @Override
    public Object getValueAt(int arg0, int arg1){
        Datos datos = lista.obtenerDato(arg0);
        switch(arg1){   
            case 0: return (arg0+1);
            case 1: return datos.getMetascore();
            case 2: return datos.getImdbRating();
            case 3: return datos.getImdbVotes();
            case 4: return datos.getImdbID();
            case 5: return datos.getType();
            case 6: return datos.getDVD();
            case 7: return datos.getBoxOffice();
            case 8: return datos.getProduction();
            case 9: return datos.getWebsite();
            case 10: return datos.getResponse();
            default: return null; 
        }
    }
    
}
