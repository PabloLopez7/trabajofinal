/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Vista.Tabla;

import Controlador.ListaEnlazadaServices;
import Modelo.Pelicula;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author Usuario
 */
public class TablaPeliculas extends AbstractTableModel{
    private ListaEnlazadaServices<Pelicula> lista;

    public ListaEnlazadaServices<Pelicula> getLista() {
        return lista;
    }

    public void setLista(ListaEnlazadaServices<Pelicula> lista) {
        this.lista = lista;
    }
    @Override
    public int getColumnCount(){
        return 14;
    }
    
    @Override
    public int getRowCount(){
        return lista.getSize();
    }
    
    @Override
    public String getColumnName(int column){
        switch(column){
            
            case 0: return "Titile";
            case 1: return "Year";
            case 2: return "Rated";
            case 3: return "Released";
            case 4: return "Runtime";
            case 5: return "Genre";
            case 6: return "Director";
            case 7: return "Writer";
            case 8: return "Actors";
            case 9: return "Plot";
            case 10: return "Lenguaje";
            case 11: return "Country";
            case 12: return "Awards";
            case 13: return "Poster";
            default: return null; 
        }
    }
    
    @Override
    public Object getValueAt(int arg0, int arg1){
        Pelicula pelicula = lista.obtenerDato(arg0);
        switch(arg1){
            
            case 0: return pelicula.getTitle();
            case 1: return pelicula.getYear();
            case 2: return pelicula.getRated();
            case 3: return pelicula.getReleased();
            case 4: return pelicula.getRuntime();
            case 5: return pelicula.getGenre();
            case 6: return pelicula.getDirector();
            case 7: return pelicula.getWrite();
            case 8: return pelicula.getActors();
            case 9: return pelicula.getPlot();
            case 10: return pelicula.getLanguaje();
            case 11: return pelicula.getCountry();
            case 12: return pelicula.getAwards();
            case 13: return pelicula.getPoster();
            default: return null; 
        }
    }
    
}
